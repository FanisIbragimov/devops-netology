output "organizations_account_id" {
  value       = "${join("", aws_organizations_account.default.*.id)}"
  description = "The AWS account id."
}
output "user_unique_id" {
  description = "The unique ID assigned by AWS"
  value       = join("", aws_iam_user.default.*.unique_id)
}
output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.app_server.public_ip
}
output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = aws_subnet.public.*.id
}
